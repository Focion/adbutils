# -*- coding: utf-8 -*-

import os
import time

adb = 'adb'
adb_device = 'adb -s '
shell = ' shell '
pull = ' pull '
install = ' install '
uninstall = ' uninstall '
space = ' '

# 截图命令
capture = '/system/bin/screencap'
capture_path = '/sdcard/Screenshots/'
capture_prefix = 'cap_'
capture_suffix = '.png'

# 数据库
db_path = '/sdcard/Android/data/com.shishike.calm/calm/databases'
db_prefix = 'db_'

# 输入命令
input_text = 'input text '
input_tap = 'input tap '


# 文件名无后缀
def __file_extra(prefix):
    return __file(prefix, '')


# 文件名
def __file(prefix, suffix):
    filename = prefix
    filename += time.strftime('%Y-%m-%d_%H-%M-%S', time.localtime())
    return filename.__add__(suffix)


# 创建文件夹
def __mkdir(path):
    is_exist = os.path.exists(path)
    if not is_exist:
        os.makedirs(path)


# adb命令
def __adb_device(device):
    return adb_device.__add__(device)


# shell命令
def __shell(device):
    return __adb_device(device).__add__(shell)


# pull命令
def __pull(device):
    return __adb_device(device).__add__(pull)


# install命令
def __install(device):
    return __adb_device(device).__add__(install)


# unstall命令
def __uninstall(device):
    return __adb_device(device).__add__(uninstall)


# 创建文件夹
def __mkdir_p(device, path):
    cmd_mkdir = __shell(device).__add__('mkdir ').__add__('-p ').__add__(path)
    print_cmd(cmd_mkdir)
    os.system(cmd_mkdir)


# 执行打印
def print_cmd(cmd):
    print('执行命令：'.__add__(cmd))


# 处理 devies
def convert_device(param):
    params = param.split('\t')
    return params[0]


# 获取设备编号
def devices():
    cmd_device = adb.__add__(' devices')
    print_cmd(cmd_device)
    results = os.popen(cmd_device).readlines()
    # 去掉标题和最后一个回车
    results.__delitem__(0)
    results.__delitem__(-1)
    size = results.__len__()
    if size <= 0:
        return ''
    elif size == 1:
        results[0] = convert_device(results[0])
        return results
    else:
        for index in range(len(results)):
            results[index] = convert_device(results[index])
        return results


# 截图
def capture_pull(device, path):
    __mkdir(path)

    file_name = __file(capture_prefix, capture_suffix)

    __mkdir_p(device, capture_path)

    step1 = __shell(device).__add__(capture).__add__(' -p ')
    step2 = capture_path.__add__(file_name)
    cmd_capture = step1.__add__(step2)
    print_cmd(cmd_capture)

    step1 = __pull(device).__add__(capture_path).__add__(file_name)
    step2 = space.__add__(path).__add__(file_name)
    cmd_pull = step1.__add__(step2)
    print_cmd(cmd_pull)

    os.system(cmd_capture)
    os.system(cmd_pull)


# 拷贝数据库
def database_pull(device, path):
    path += __file_extra(db_prefix)
    __mkdir(path)

    step1 = __pull(device).__add__(db_path)
    step2 = space.__add__(path)
    cmd_pull = step1.__add__(step2)
    print_cmd(cmd_pull)

    os.system(cmd_pull)


# 拷贝文件
def file_pull(device, from_path, to_path, file):
    __mkdir(to_path)

    step1 = __pull(device).__add__(from_path).__add__('/').__add__(file)
    step2 = space.__add__(to_path).__add__(file)
    cmd_pull = step1.__add__(step2)
    print(cmd_pull)

    os.system(cmd_pull)


# 拷贝文件夹
def files_pull(device, from_path, to_path):
    __mkdir(to_path)

    paths = from_path.split('/')

    step1 = __pull(device).__add__(from_path)
    step2 = space.__add__(to_path).__add__('\\').__add__(paths[-1])
    cmd_pull = step1.__add__(step2)
    print(cmd_pull)

    os.system(cmd_pull)


# 安装应用
def install_apk(device, path, file):
    step1 = __install(device).__add__(path)
    step2 = '\\'.__add__(file).__add__('.apk')
    cmd_install = step1.__add__(step2)
    print_cmd(cmd_install)

    os.system(cmd_install)


# 卸载应用
def uninstall_apk(device, package):
    cmd_uninstall = __uninstall(device).__add__(package)
    print_cmd(cmd_uninstall)

    os.system(cmd_uninstall)


# 输入内容
def input_msg(device, content):
    cmd_input_text = __shell(device).__add__(input_text).__add__(content)
    print_cmd(cmd_input_text)

    os.system(cmd_input_text)


# 点击操作
def click(device, x, y):
    step1 = __shell(device).__add__(input_tap)
    step2 = x.__str__().__add__(space).__add__(y.__str__())
    cmd_input_tap = step1.__add__(step2)
    return cmd_input_tap


# 输入内容
def msg(device, content):
    return __shell(device).__add__(input_text).__add__(content)
