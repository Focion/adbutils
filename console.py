# -*- coding: utf-8 -*-
import adb_lib as adb
import sign_lib as sign

input('欢迎使用adb命令集合，按回车继续')


# 回车
def println():
    print('\n')


# 操作成功
def print_sucess():
    print('----- 执行成功 -----\n')


# 输入设备号
def input_device():
    return input('请选择输入设备号：')


# 返回菜单判断
def back(content):
    return '0'.__eq__(content)


device = ''


# 检查设备
def check_device():
    return ''.__eq__(device)


while True:
    if check_device():
        print('\n准备绑定设备...')
        results = adb.devices()
        size = results.__len__()
        if size == 0:
            input('未检测到可用设备，按任意键退出')
            break
        elif size == 1:
            device = results[0]
            print('设备已绑定，设备号：', device)
        else:
            print(results)
            index = int(input_device())
            if size >= index > 0:
                device = results[index - 1]
                print('设备已绑定，设备号：', device)
            else:
                print('----- 选择设备错误 -----')
                continue

    print('\n----- 主菜单 -----\n')
    menus = ('1.重新绑定设备', '2.安装应用', '3.卸载应用', '4.截图', '5.拷贝数据库', '6.输入内容', '7.拷贝文件', '8.拷贝文件夹', '0.退出\n')
    for menu in menus:
        print(menu)
    numbers = ['-1', '0', '1', '2', '3', '4', '5', '6', '7', '8']
    number = input('请输入你想要操作的序号：')
    if number in numbers:
        num = int(number)
    else:
        print('----- 输入错误! -----\n')
        continue

    println()

    if num == 1:
        device = ''
        continue
    elif num == 2:
        path = input('请输入Apk所在本地路径：')
        if back(path):
            continue

        file = input('请输入Apk文件名：')
        if back(file):
            continue

        adb.install_apk(device, path, file)
    elif num == 3:
        package = input('请输入卸载包名：')
        if back(package):
            continue

        adb.uninstall_apk(device, package)
    elif num == 4:
        path = input('请输入保存到本地路径（以\'\\\'结尾）：')
        if back(path):
            continue

        adb.capture_pull(device, path)
    elif num == 5:
        path = input('请输入保存到本地路径（以\'\\\'结尾）：')
        if back(path):
            continue

        adb.database_pull(device, path)
    elif num == 6:
        msg = input('请输入内容：')
        if back(msg):
            continue

        adb.input_msg(device, msg)
    elif num == 7:
        from_path = input('请输入设备文件路径：')
        if back(from_path):
            continue

        to_path = input('请输入保存到本地路径（以\'\\\'结尾）：')
        if back(to_path):
            continue

        file_with_suiffx = input('请输入文件名（包括扩展名）：')
        if back(file_with_suiffx):
            continue

        adb.file_pull(device, from_path, to_path, file_with_suiffx)
    elif num == 8:
        from_path = input('请输入设备文件夹路径：')
        if back(from_path):
            continue

        to_path = input('请输入保存到本地路径（以\'\\\'结尾）：')
        if back(to_path):
            continue

        adb.files_pull(device, from_path, to_path)
    elif num == -1:
        print('----- 隐藏打卡功能 -----\n')
        username = input('请输入用户名：')
        if back(username):
            continue

        password = input('请输入密码：')
        if back(password):
            continue

        clocks = input('请设置打卡时间（如：\'18:00\'）：').split(':')
        if clocks.__len__() != 2:
            continue

        sign.prepare(device, username, password)
        sign.start(int(clocks[0]), int(clocks[1]))
    elif num == 0:
        break
