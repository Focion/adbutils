# -*- coding: utf-8 -*-
import datetime
import os
import time

device = '192.168.62.101:5555'
username = '18615785163'
password = '123456'

constants = 'adb -s ' + device + ' shell '

step0 = constants + 'input tap 553 1055'
step1 = constants + 'input tap 130 466'

step2 = constants + 'input tap 142 1022'
step3_1 = constants + 'input tap 892 1020'
step3_2 = constants + 'input text ' + username

step4 = constants + 'input tap 106 1214'
step5 = constants + 'input text ' + password

step6 = constants + 'input tap 351 1473'

step7 = constants + 'input tap 160 1000'

step8 = constants + 'input tap 550 1475'

step9 = constants + 'input tap 281 1849'

step10 = constants + 'input tap 904 1729'

step11 = constants + 'input tap 1009 111'

step12 = constants + 'input tap 695 751'

step13 = constants + 'input tap 748 972'

step14 = constants + 'input tap 281 1849'

step15 = constants + 'input tap 537 1859'

nums1 = [step0, step1, step2, step3_1, step3_2, step4, step5, step6]

nums2 = [step7, step8, step9, step10, step11, step12, step13, step14, step15]


def __task():
    for num in nums1:
        print(num)
        os.system(num)
        time.sleep(5)

    time.sleep(10)

    for num in nums2:
        print(num)
        os.system(num)
        time.sleep(5)


def main(h, m):
    while True:
        while True:
            now = datetime.datetime.now()
            print(now.hour, ':', now.minute, ':', now.second)
            # 到达设定时间，结束内循环
            if now.hour == h and now.minute == m:
                break
            # 不到时间就等20秒之后再次检测
            time.sleep(1)
            # 做正事，一天做一次
        __task()


main(18, 26)
