# -*- coding: utf-8 -*-
import datetime
import os
import time

import adb_lib as adb

steps = []

delay_steps = []


def prepare(device, username, password):
    steps.append(adb.click(device, 533, 1055))
    steps.append(adb.click(device, 130, 466))
    steps.append(adb.click(device, 142, 1022))
    steps.append(adb.click(device, 892, 1020))
    steps.append(adb.msg(device, username))
    steps.append(adb.click(device, 106, 1214))
    steps.append(adb.msg(device, password))
    steps.append(adb.click(device, 351, 1473))

    delay_steps.append(adb.click(device, 160, 1000))
    delay_steps.append(adb.click(device, 550, 1475))
    delay_steps.append(adb.click(device, 281, 1849))
    delay_steps.append(adb.click(device, 904, 1729))
    delay_steps.append(adb.click(device, 1009, 111))
    delay_steps.append(adb.click(device, 695, 751))
    delay_steps.append(adb.click(device, 748, 972))
    delay_steps.append(adb.click(device, 281, 1849))
    delay_steps.append(adb.click(device, 537, 1859))


def task():
    for step in steps:
        adb.print_cmd(step)
        os.system(step)
        time.sleep(5)

    time.sleep(10)

    for step in delay_steps:
        adb.print_cmd(step)
        os.system(step)
        time.sleep(5)


def start(h, m):
    while True:
        while True:
            now = datetime.datetime.now()
            print(now.hour, ':', now.minute, ':', now.second)
            if now.hour == h and now.minute == m:
                print(now.hour, ':', now.minute, ':', now.second)
                break
            time.sleep(5)
        task()
